import speech_recognition as sr
import pyttsx3
import requests
import spacy
from datetime import datetime, timedelta, date
import re

# Load the spaCy English model
nlp = spacy.load("en_core_web_sm")

# Initialize the recognizer
r = sr.Recognizer()

# Initialize the text-to-speech engine
engine = pyttsx3.init()

# Define API endpoints for doctor availability
get_user_url = 'http://localhost:5000/users'
add_user_url = 'http://localhost:5000/users'
get_availability_url = 'http://localhost:5000/get_appointments'
add_availability_url = 'http://localhost:5000/add_availability'
remove_availability_url = 'http://localhost:5000/remove_availability'
schedule_appointment_url = 'http://localhost:5000/schedule_appointment'

# Use the default microphone as the audio source
with sr.Microphone() as source:
    print("Listening...")

    # Adjust for ambient noise
    r.adjust_for_ambient_noise(source)

    # Listen for the user's voice input
    audio = r.listen(source)

# Convert the audio to text
try:
    # text = r.recognize_google(audio)

    available_appointments = []

    while(len(available_appointments) == 0):
        next_target_day = None
        while next_target_day == None:
            text = "Hello, I want to schedule an appointment for teeth cleaning for tomorrow"
            print(f"You said: {text}")
            print("Bot: Sure, just give me a second.")

            doc = nlp(text)
            entities = [ent.text for ent in doc.ents]

            current_date = datetime.now().date()
            target_day_str = ""
            if(len(entities) > 0):
                target_day_str = entities[0]


            # If monday to sunday is present
            day_name_to_int = {
                'monday': 0,
                'tuesday': 1,
                'wednesday': 2,
                'thursday': 3,
                'friday': 4,
                'saturday': 5,
                'sunday': 6
            }
            month_map = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
            pattern = r'(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)\b'
            matches = re.findall(pattern, target_day_str, re.IGNORECASE)

            
            if(target_day_str.lower() in day_name_to_int):
                target_day_int = day_name_to_int[target_day_str]
                days_until_target_day = (target_day_int - current_date.weekday() + 7) % 7
                next_target_day = current_date + timedelta(days=days_until_target_day)
            elif (len(matches) > 0 and matches[0] in month_map):
                current_year = datetime.now().year
                date_string_with_year = f"{target_day_str} {current_year}"
                format_string = "%d %B %Y"
                next_target_day = datetime.strptime(date_string_with_year, format_string)
                if(next_target_day.date() < current_date):
                    if(current_date.year % 4 == 0):
                        next_target_day = next_target_day + timedelta(days=366)
                    else:
                        next_target_day = next_target_day + timedelta(days=365)
            elif (target_day_str.lower() == 'tomorrow'):
                next_target_day = current_date + timedelta(days=1)
            elif (target_day_str.lower() == 'today'):
                next_target_day = current_date
            # print(next_target_day)

        param = '?date=' + next_target_day.strftime("%Y-%m-%d")
        response = requests.get(get_availability_url+param)
        available_appointments = response.json()['appointments']
        available_time_slots = {}
        slots_string = "Bot: Available slots for " + next_target_day.strftime("%a, %d %B") + " are "
        for i in range(0, len(available_appointments)):
            current_slot_in_str = available_appointments[i]['available_time']
            parsed_current_slot = datetime.strptime(current_slot_in_str, '%a, %d %b %Y %H:%M:%S %Z')
            slots_string += parsed_current_slot.strftime('%H:%M') + " "
            available_time_slots[parsed_current_slot.time()] = parsed_current_slot.strftime('%Y-%m-%d %H:%M:%S')

        if(len(available_appointments) == 0):
            print("Bot: There are no slots available for " + next_target_day.strftime("%a, %d %B"))
            print("Bot: Please try to book for another date?")

    # else:
    print(slots_string + "When would you like to come in?")

    # text = "6am works Great!"
    # print(f"You said: {text}")
    doc = nlp(text)
    entities = [ent.text for ent in doc.ents]
    # print(entities)

    time_string = entities[0]
    time_object = datetime.strptime(time_string, "%I%p").time()

    # print("You said: Yes, its Yukta and my phone number is 8983302399")
    while (time_object not in available_time_slots):
        print("Bot: Sorry! slot is not available for your time!!!")
        print(slots_string + "Please select slot from these")
        # text = "5am works Great!"
        # print(f"You said: {text}")
        doc = nlp(text)
        entities = [ent.text for ent in doc.ents]
        # print(entities)
        time_string = entities[0]
        time_object = datetime.strptime(time_string, "%I%p").time()

    # if(time_object in available_time_slots):
    print("Bot: Okay great! Can I get your phone number and Name?")
    # text = "Yes, its Yukta and my phone number is 8983302399"
    # print(f"You said: {text}")
    doc = nlp(text)
    entities = [ent.text for ent in doc.ents]
    name = entities[0]
    phone = entities[1]
    user_id = None
    response = requests.get(get_user_url+'?phone='+phone)
    # print(response.status_code)

    if(response.status_code == 404):
        data = {'name': name, 'phone': phone}
        response = requests.post(add_user_url, json=data)
        print(response.json()['message'])
        response = requests.get(get_user_url+'?phone='+phone)
        
    user_id = response.json()['id']
    time_slot = available_time_slots[time_object]
    data = {'time_slot': time_slot, 'user_id': user_id}
    response = requests.post(schedule_appointment_url, json=data)
    # print(response.json()['message'])
    print("Bot: Awesome. I have you set up for that time. To cancel or reschedule please call us again. See you soon.")


    # else:
    #     print("Bot: Sorry! slot is not available for your time!!!")   

    # Process the user's request based on the text
    # if "schedule an appointment" in text:
        # Call the API to add doctor availability
        # Add code here to extract doctor name and available time from the user's request
        # data = {'doctor_name': 'Dr. Archer', 'available_time': '10 am'}
        # response = requests.post(add_availability_url, json=data)
        # print(response.json()['message'])
    # elif "cancel appointment" in text:
    #     # Call the API to remove doctor availability
    #     # Add code here to extract doctor name and available time from the user's request
    #     data = {'doctor_name': 'Dr. Archer', 'available_time': '10 am'}
    #     response = requests.post(remove_availability_url, json=data)
    #     print(response.json()['message'])
    # else:
    #     print("Sorry, I couldn't understand that.")

except sr.UnknownValueError:
    print("Sorry, I couldn't understand that.")
except sr.RequestError:
    print("Sorry, there was an error with the request.")
