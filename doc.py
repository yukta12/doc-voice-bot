from flask import Flask, request, jsonify
import mysql.connector
import json
from datetime import datetime, timedelta, date

app = Flask(__name__)

# Connect to the MySQL server
conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='password',
    database='doc_appt'
)
c = conn.cursor()

@app.route('/get_appointments', methods=['GET'])
def get_appointments():
    start_date = request.args.get('date')
    parsed_date = datetime.strptime(start_date, '%Y-%m-%d')
    next_date = parsed_date + timedelta(days=1)
    appointments = []
    c.execute("SELECT * FROM doctor_availability WHERE user_id IS NULL AND available_time >= %s AND available_time < %s", (start_date, next_date))
    rows = c.fetchall()
    for row in rows:
        appointment = {
            'id': row[0],
            'available_time': row[1],
            'user_id': row[2]
        }
        appointments.append(appointment)

    return jsonify({'appointments': appointments})

@app.route('/add_availability', methods=['POST'])
def add_appointment():
    data = request.json

    for i in range(0, len(data)):
        available_time = data[i]['available_time']
        c.execute("INSERT INTO doctor_availability (available_time) VALUES (%s)", ([available_time]))

    conn.commit()

    return jsonify({'message': 'Appointment added successfully'})

@app.route('/remove_availability', methods=['POST'])
def remove_availability():
    data = request.json

    for i in range(0, len(data)):
        available_time = data[i]['available_time']
        c.execute("DELETE FROM doctor_availability WHERE available_time = %s", ([available_time]))

    conn.commit()
    conn.close()

    return jsonify({'message': 'Doctor availability removed successfully'})

@app.route('/schedule_appointment', methods=['POST'])
def schedule_appointment():
    data = request.json
    # for i in range(0, len(data)):
    time_slot = data['time_slot']
    user_id = data['user_id']
    # c.execute("INSERT INTO doctor_availability (available_time) VALUES (%s)", ([available_time]))
    c.execute("UPDATE doctor_availability SET user_id = %s, available_time = %s WHERE available_time = %s", (user_id, time_slot, time_slot))

    conn.commit()

    return jsonify({'message': 'Appointment scheduled successfully'})

@app.route('/users', methods=['POST'])
def create_record():
    data = request.json
    name = data['name']
    phone = data['phone']
    
    c.execute("INSERT INTO users (name, phone) VALUES (%s, %s)", (name, phone))
    conn.commit()
    
    return jsonify({'message': 'Record created successfully'}), 201

# Read Operation
@app.route('/users', methods=['GET'])
def read_record():
    phone = request.args.get('phone')
    print(phone)
    if(phone != None):
        c.execute("SELECT * FROM users WHERE phone=%s", (phone,))
        record = c.fetchone()
        
        if record:
            return jsonify({'id': record[0], 'name': record[1], 'phone': record[2]}), 200
        else:
            return jsonify({'message': 'Record not found'}), 404
    

    c.execute("SELECT * FROM users")
    rows = c.fetchall()
    records = []
    for row in rows:
        record = {
            'id': row[0],
            'name': row[1],
            'phone': row[2]
        }
        records.append(record)
    return jsonify({'users': records}), 200


if __name__ == '__main__':
    app.run(debug=True)