# Voice Bot for Scheduling Doctor Appointments

## Overview

This project implements a voice bot that responds to patient questions about scheduling appointments with a doctor. The bot provides the following actions for patients and doctors:

### Voice Bot Actions

- **Schedule an Appointment**: Allows patients to schedule a new appointment.
- **Reschedule an Appointment**: Allows patients to reschedule an existing appointment.

### Doctor Actions

- **Expose an API**: Allows doctors to manage available time slots for appointments.
@app.route('/get_appointments', methods=['GET'])
@app.route('/add_availability', methods=['POST'])
@app.route('/remove_availability', methods=['POST'])
@app.route('/schedule_appointment', methods=['POST'])
@app.route('/users', methods=['POST'])
@app.route('/users', methods=['GET'])

## Getting Started

To run the voice bot and doctor API, follow these steps:

install all the dependencies, run conn.py and then bot.py 


### Voice Bot

To interact with the voice bot, follow these voice commands:


Nickk's Pa@NicKPC MING /64 ~/Documents/yukta/DocVoiceBot (master)
$ python bot.py
Listening..
You said: Hello, I want to schedule an appointment for teeth cleaning for tomorrow
Bot: Sure, just give me a second.
You said: Okay
Bot: Available slots for Mon, 04 March are 07:00 When would you like to come in?
You said: 7am works Great!
Bot: Okay great! Can I get your phone number and Name?
You said: Yes, its Yukta and my phone number is 8983302399
 Bot: Awesome.
I have you set up for that time.
To cancel or reschedule please call us again. See you soon.

