import mysql.connector

# Connect to the MySQL server
conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='password',
    database='doc_appt'
)
c = conn.cursor()

# Create a table for the doctor's available time slots
c.execute('''CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    phone VARCHAR(10) NOT NULL
  )''')
c.execute('''CREATE TABLE IF NOT EXISTS doctor_availability (
    id INT AUTO_INCREMENT PRIMARY KEY,
    available_time TIMESTAMP NOT NULL,
    user_id INT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
)''')

conn.commit()

# Close the connection
conn.close()
